LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_INSTALL_MODULES:=on
OPENCV_CAMERA_MODULES:=off
OPENCV_LIB_TYPE:=STATIC

include ${OPENCV}\sdk\native\jni\OpenCV.mk

LOCAL_MODULE    := jni_extractPose
LOCAL_SRC_FILES := jni_extractPose.cpp scanraster.cpp raster3d.cpp
LOCAL_LDLIBS +=  -llog -ldl
LOCAL_CFLAGS := -std=gnu++11

include $(BUILD_SHARED_LIBRARY)
