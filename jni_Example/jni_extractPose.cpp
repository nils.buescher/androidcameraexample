/*
 * Copyright (C) 2015 Bosch Sensortec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctime>
#include <chrono>
#include <jni.h>
#include <opencv2/core/types_c.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <algorithm>

#include "scanraster.h"
#include "raster3d.h"

using namespace cv;

#include <android/log.h>
#define log(...) __android_log_print(ANDROID_LOG_INFO, "SensorTest", __VA_ARGS__)
#define logTimes(...) __android_log_print(ANDROID_LOG_INFO, "SensorTest_Times", __VA_ARGS__)

extern "C"
{
    JNIEXPORT jdoubleArray JNICALL Java_org_memsscore_evaluator_ImageProcessing_ExtractPose_getPoseFromRaster(JNIEnv* env, jobject obj, jbyteArray image, jint width, jint height, jdoubleArray projection, jfloat quadSize)
    {

        Quad quads[9];

        // Check arguments
        if ((image == NULL) || (width <= 0) || (height <= 0))
            return NULL;

        // Check the array for the position
        if ((projection == NULL) || (env->GetArrayLength(projection) != 9))
            return NULL;

        // Read Java Array
        jbyte* image_ptr = env->GetByteArrayElements(image, NULL);
        if (image_ptr == NULL)
            return NULL;

        Mat input = Mat(height, width, CV_8UC1, image_ptr);

        // Find 2D Quads
        int quad_count;

        //--------------scan the raster----------------
        
        quad_count = scan_raster(&input, quads, 9);
        env->ReleaseByteArrayElements(image, image_ptr, JNI_ABORT);

        // No quads have been found. Exist and return null.
        if (quad_count == 0)
            return NULL;

        //--------------calculate 3d rotation----------------
        
        jdouble p[9];
        env->GetDoubleArrayRegion(projection, 0, 9, p);

        Raster3D raster;

        // Get the raster
        if (!get_raster_3d(quads, quad_count, p, &raster, quadSize)) {
            return NULL;
        }

        // Calculate rotation matrix 
        jdouble r[18];

        get_pose(&raster, r);

        // Copy result into Java Array (double[]) 
        jdoubleArray rect = env->NewDoubleArray(18);
        for (int i = 0; i < 18; i++)
        {
            env->SetDoubleArrayRegion(rect, i, 1, &r[i]);
        }

        return rect;
    }
}
