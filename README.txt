The application needs granted access to the camera to run. It will crash otherwise.

The access to the camera has to be allowed for the application in the settings.

To grand the user access to the camera directly in the application please see: https://developer.android.com/guide/topics/permissions/overview and https://developer.android.com/training/permissions/requesting