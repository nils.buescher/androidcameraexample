package rostock.uni.cameratestapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    //The camera to capture the images
    private Camera mCamera;

    //The listener for images
    private ImageListener mImageListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get the texture view
        TextureView preview = findViewById(R.id.textureView);
        mImageListener = new ImageListener();

        mCamera = new Camera(this);
        mCamera.setFlashLight(false);
        mCamera.setResolution(800,600);
        mCamera.setImageListener(mImageListener);
        mCamera.setSurface(preview);

        // Example of a call to a native method
        //String testString = stringFromJNI();
    }

    /**
     * When the app is paused
     */
    @Override
    public void onPause() {
        super.onPause();
        mCamera.onPause();
    }

    /**
     * When the app is resumed
     */
    @Override
    public void onResume() {
        super.onResume();
        mCamera.onResume();
    }

    /**
     * When the app is destroyed
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mCamera.onDestroy();
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

}
