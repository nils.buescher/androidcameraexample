/*
 * Copyright (C) 2015 Bosch Sensortec GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rostock.uni.cameratestapplication;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.WindowManager;

/**
 * The Camera class produces a stream of images and shows a preview in a
 * SurfaceView. In addition, there are methods for setting the resolution and
 * activating the flash light of the camera. All methods for setting the
 * resolution and the preview surface can be called in any order.
 * <p>
 * As all data sources, the camera participates in Android life-cycle via the
 * onResume() and onPause() methods. Since only one application can hold the
 * camera, these methods should be called in response to the activity events.
 * <p>
 * This class utilizes the first back-facing camera in the phone. If the camera
 * is not available, no images are produced. In case of an error, the camera
 * generates test red images and displays an appropriate message in the preview.
 * Hence, subsequent consumer stages do not require special error handling.
 */
@SuppressWarnings("deprecation")
public class Camera {

	/** The context of the application */
	private Context context;
	
	/** Stores whether the camera is reversed*/
	private boolean reversed;
	
	/** Reference to the Android camera. */
	private android.hardware.Camera camera;

	/** External SurfaceView for the preview */
	private TextureView surface;

	/** Internal callback to the reference to the surface. */
	private SurfaceTextureCallback surfaceTextureCallback;

	/** Internal callback, which receives preview images. */
	private Preview preview;

	/** Determines if the camera is on or off. */
	private boolean flashLight;

	/** Requested width of the images to be produced. */
	private int width;

	/** Requested height of the images to be produced. */
	private int height;

	/** Camera field-of-view in degrees, attached to the produces images. */
	private float fov;

	/** Focal length of the camera in mm, attached to the produces images. */
	private float focalLength;

	/** Listener for new images */
	private ImageListener mListener;

	//-----------------------------------------
	//region 		Life cycle
	//-----------------------------------------

	/**
	 * Constructs a new instance of the camera class.
	 */
	Camera(Context context) {
        preview = new Preview();
        this.context = context;

        CameraInfo info = getCameraInfo();
        reversed = info != null && (getCameraInfo().orientation >= 180);
    }

	/**
	 * Opens the camera and performs all necessary initializations. This method
	 * should be called from the activity in response to onResume events.
	 */
	public void onResume() {
		if (camera != null) {
			return;
		}

		// The Android camera is sensitive to the ordering of these operations.

		//Open the camera
		camera = android.hardware.Camera.open();

		if (camera != null) {

			//Assign stored parameters to the camera.
			Parameters p = camera.getParameters();
			p.setPreviewFormat(ImageFormat.NV21);
			setPreviewSize(p, width, height);

			//Activate the LED if required.
			p.setFlashMode(flashLight ? Parameters.FLASH_MODE_TORCH : Parameters.FLASH_MODE_OFF);

			fov = p.getHorizontalViewAngle();
			focalLength = p.getFocalLength();
			
			camera.setParameters(p);

			//Set the orientation of the camera
			int rotation = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation(); //getWindowManager().getDefaultDisplay()
		             
		     int degrees = 0;
		     switch (rotation) {
		         case Surface.ROTATION_0: degrees = 0; break;
		         case Surface.ROTATION_90: degrees = 90; break;
		         case Surface.ROTATION_180: degrees = 180; break;
		         case Surface.ROTATION_270: degrees = 270; break;
		     }

            CameraInfo info = getCameraInfo();

            int result = 0;


            if (info != null) {
                if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
					result = (info.orientation + degrees) % 360;
					result = (360 - result) % 360;  // compensate the mirror
				}
				else {
					result = (getCameraInfo().orientation - degrees + 360) % 360;
				}
            }

            camera.setDisplayOrientation(result);
			
			//Attach the preview callback
			camera.setPreviewCallback(preview);
		}

		//Initialize the surface and corresponding callbacks
		if (surface != null) {
			this.surfaceTextureCallback = new SurfaceTextureCallback();
			this.surface.setSurfaceTextureListener(surfaceTextureCallback);
			
			//this.surfaceHolder.addCallback(callback);

			if (camera != null) {
				try {
					camera.setPreviewTexture(this.surface.getSurfaceTexture());//(this.surfaceHolder);
				} catch (IOException ignored) {
				}
			}
		}

		/* Start the camera preview after the surfaces are initialized. */
		if (camera != null) {
			camera.startPreview();
		}
	}

	/**
	 * Closes the camera and stops the preview as well as the production of
	 * images. After, onPause() has been called, the camera can be reused by
	 * another activity. This method should be called from the activity in
	 * response to onPause() events.
	 */
	public void onPause() {
		close();
	}

	/**
	 * When the application is destroyed, also destroy the camera and free
	 * all references
	 */
	public void onDestroy() {
		this.close();
		this.context = null;
		this.surface = null;

	}
	//endregion

	//-----------------------------------------
	//region        Public Methods
	//-----------------------------------------

	/**
	 * Sets the preview surface of this camera. The preview is not started until
	 * onResume() is called and automatically stopped after the onPause() event.
	 *
	 * @param surface
	 *            the new preview surface.
	 */
	public void setSurface(TextureView surface) {

		/* Changing the preview surface requires to stop the camera. */
		if (camera != null) {
			try {
				camera.stopPreview();
				camera.setPreviewDisplay(null);
			} catch (IOException ignored) {
			}
		}

		/* The internal callback must be detached from the old surfaceHolder. */
		if (this.surface != null) {
			if (surfaceTextureCallback != null) {
				surface.setSurfaceTextureListener(null);
			}
			this.surfaceTextureCallback = null;
			this.surface = null;
		}

		/* The reference to the surface is updated. */
		this.surface = surface;

		/* The surface holder and callbacks are recreated for the new surface. */
		if (surface != null) {
			//this.surfaceHolder = surface.getHolder();
			//this.callback = new Callback();
			this.surfaceTextureCallback = new SurfaceTextureCallback();
			this.surface.setSurfaceTextureListener(surfaceTextureCallback);

			//this.surfaceHolder.addCallback(callback);

			/* Restart the preview if the camera is active. */
			if (camera != null) {
				try {
					camera.setPreviewTexture(surface.getSurfaceTexture());
					camera.startPreview();
				} catch (IOException ignored) {
				}
			}
		}
	}

	/**
	 * Changes the resolution of the camera. If the requested resolution is not
	 * supported by the camera, this method chooses the best available match.
	 *
	 * @param width
	 *            the requested width in pixels
	 * @param height
	 *            the requested height in pixels
	 */
	public void setResolution(int width, int height) {
		this.width = width;
		this.height = height;

		/*
		 * If the camera is not open, the resolution will be set in the next
		 * onResume() event.
		 */

		if (camera != null) {
			/* Stop the preview before changing the parameters */
			camera.stopPreview();
			Parameters p = camera.getParameters();

			setPreviewSize(p, width, height);

			/* Update parameters and restart preview */
			camera.setParameters(p);
			camera.startPreview();
		}
	}

	/**
	 * Get whether the image is reversed (rotatated by 180°
	 * @return True when the image is reversed, else false
	 */
	public boolean isReversed() {
		return reversed;
	}

	/**
	 * Trigger the auto focus of the application
	 */
	public void focus() {
		if (camera != null) {
			camera.autoFocus(new android.hardware.Camera.AutoFocusCallback() {
				@Override
				public void onAutoFocus(boolean success, android.hardware.Camera camera) {
					//nothing to do here
				}
			});
		}
	}

	/**
	 * Get the info of the camera that is currently used
	 * @return the CameraInfo of the camera
	 */
	private CameraInfo getCameraInfo () {
		int numberOfCameras = android.hardware.Camera.getNumberOfCameras();
		for (int i = 0; i < numberOfCameras; i++) {
		  CameraInfo info = new CameraInfo();
		  android.hardware.Camera.getCameraInfo(i, info);
		  if (info.facing == CameraInfo.CAMERA_FACING_BACK) {
		    return info;
		  }
		}
		return null;
	}


	/**
	 * Sets the status of the camera flash light.
	 * @param active determines if the light should be on
	 */
	public void setFlashLight(boolean active) {
		this.flashLight = active;

		if (camera != null) {
			Parameters p = camera.getParameters();
			p.setFlashMode(active ? Parameters.FLASH_MODE_TORCH : Parameters.FLASH_MODE_OFF);
			camera.setParameters(p);
		}
	}

	/**
	 * Returns the status of the camera flash light.
	 */
	public boolean getFlashLight() {
		return flashLight;
	}

	/**
	 * Set the listener for images
	 * @param listener The new listener
	 */
	public void setImageListener(ImageListener listener) {
		this.mListener = listener;
	}

	/**
	 * Resets the listener for images
	 * Equal to setImageListener(null)
	 */
	public void resetImageListener() {
		this.mListener = null;
	}



	//endregion

	//-----------------------------------------
	//region        Helper Methods
	//-----------------------------------------

	/**
	 * Update the texture matrix
	 * @param width The width of the image
	 * @param height The height of the image
	 */
	private void updateTextureMatrix(int width, int height)
	{
		int previewWidth = this.width;
		int previewHeight = this.height;

		float ratioSurface = (float) width / height;
		float ratioPreview = (float) previewWidth / previewHeight;

		float scaleX;
		float scaleY;

		if (ratioSurface > ratioPreview)
		{
			scaleX = ratioPreview/ratioSurface;
			scaleY = 1;
		}
		else
		{
			scaleX = 1;
			scaleY = ratioPreview/ratioSurface;
		}

		Matrix matrix = new Matrix();

		matrix.setScale(scaleX, scaleY);
		surface.setTransform(matrix);

		float scaledWidth = width * scaleX;
		float scaledHeight = height * scaleY;

		float dx = (width - scaledWidth) / 2;
		float dy = (height - scaledHeight) / 2;
		surface.setTranslationX(dx);
		surface.setTranslationY(dy);
	}

	/**
	 * Closes the camera and returns all the resources
	 */
	private void close() {
		/*
		 * The camera must be shut down carefully. An invalid sequence of
		 * operations might lead to an exception.
		 */

		/* First, remove the callback. */
		if ((surface != null)) {
			surface.setSurfaceTextureListener(null);
		}

		if (surfaceTextureCallback != null) {
			surfaceTextureCallback = null;
		}

		/* Deactivate the callback and the preview. */
		if (camera != null) {
			try {
				camera.setPreviewCallback(null);
				camera.setPreviewDisplay(null);
			} catch (IOException ignored) {
			}
			camera.release();
			camera = null;
		}
	}

	/**
	 * Sets the size of the camera preview. If the requested resolution is not
	 * supported by the camera, this method chooses the best available match.
	 * 
	 * @param parameters
	 *            the camera parameters, which should be updated
	 * @param width
	 *            the requested width in pixels
	 * @param height
	 *            the requested height in pixels
	 */
	private void setPreviewSize(Parameters parameters, int width, int height) {

		/*
		 * When setting an unsupported resolution, the camera throws an
		 * exception.
		 */

		/* Zero is uninitialized and will be ignored. */
		if ((width == 0) || (height == 0)) {
			return;
		}

		/* Get a list of all supported resolutions. */
		List<Size> list = parameters.getSupportedPreviewSizes();

		/* Choose the best resolution. */
		Size bestSize = null;
		int minError = 0;

		for (Size s : list) {
			int error = Math.abs(s.width - width) + Math.abs(s.height - height);
			if ((error < minError) || (bestSize == null)) {
				bestSize = s;
				minError = error;
			}
		}

		/* Only if a best resolution has been found, apply it to the parameters. */
		if (bestSize != null) {
			parameters.setPreviewSize(bestSize.width, bestSize.height);
		}
	}

	/**
	 * Processes a camera frame by sending it to all connected consumers.
	 * Derived classes can override this method implement additional
	 * functionality. The data array belongs to the camera and should not be
	 * modified.
	 *
	 * @param data
	 *            the image data in 8-bit grey-scale format
	 * @param width
	 *            the width of the image in pixels
	 * @param height
	 *            the height of the image in pixels
	 */
	private void onImageData(byte[] data, int width, int height) {

		if (mListener != null) {
			mListener.onImage(data, width, height, fov, focalLength, System.currentTimeMillis());
		}
	}

	//endregion

	//-----------------------------------------
	//region        Internal Classes
	//-----------------------------------------

	/**
	 * The TextureSurface callback receives notifications about the creation and
	 * destruction of the preview SurfaceTexture because it might not be available when
	 * the camera is created. Thus, this callback has to attach and detach the
	 * surface to the camera preview accordingly.
	 */
    private class SurfaceTextureCallback implements TextureView.SurfaceTextureListener{

		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
			/* The surface has been created and can be attached to the camera. */

			/*
			 * The callback does not belong to this camera anymore. It might
			 * have been replaced ro set to zero, so this event is ignored.
			 */
			if (surfaceTextureCallback != this)
				return;
			
			/*
			 * If the camera is initialized, the newly created surface can be
			 * set as the preview surface.
			 */
			if (camera != null) {
				try {
					camera.setPreviewTexture(surface.getSurfaceTexture());
					updateTextureMatrix(width, height);
				} catch (IOException ignored) {
				}
			}
		}
		
		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
			/* This event should not happen, since we controlling the surface. */
			updateTextureMatrix(width, height);
		}
		
		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
			/* Remove it from the camera. */
			try {
				if (camera != null) {
					camera.setPreviewDisplay(null);
				}
			} catch (IOException e) {
				/* Since we are closing anyway, ignore errors. */
			}
			
			return true;
		}
		
		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture texture) {
		}
			//nothing to do here
		}

    /**
	 * The preview callback receives camera images.
	 */
	private class Preview implements PreviewCallback {
		@Override
		public void onPreviewFrame(byte[] data, android.hardware.Camera camera) {
			/* Check if the camera is valid and the frame contains data. */
			if ((camera != null) && (data != null)) {
				/* capture size */
				android.hardware.Camera.Size size = camera.getParameters().getPreviewSize();
				onImageData(data, size.width, size.height);
			}
		}
	}

	//endregion
}
